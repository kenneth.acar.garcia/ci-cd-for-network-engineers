## CI/CD for Network Engineers

This project is used as a demostration of the CI/CD pipeline concepts for network engineers. This is more of entry-level project instead of an extensive guide on automation. Concepts from here can be used across various other automation tools as well as platforms. 

\
\
Anurag Bhatia \
Hurricane Electric




### Topology

| Device A                    | Device B         |
| --------------------------- | ---------------- |
| Red (203.0.113.1/30) | R1 (203.0.113.2/30)     |
| R1 (203.0.113.5/30)  | R2 (203.0.113.6/30)     |
| R2 (203.0.113.9/30)  | Green (203.0.113.10/30) |
| R1 (203.0.113.13/30) | C (203.0.113.14/30)     |

![Screenshot_20211123_221504.png](./Screenshot_20211123_221504.png)


This is presented at APRICOT 2022 as a tutorial. Slides [here](https://docs.google.com/presentation/d/1MHVYuFV8F-KWqA9GIHWk4QeXA1JXubYryKfz6AcTFjk/edit?usp=sharing)
